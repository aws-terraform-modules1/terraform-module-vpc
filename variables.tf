variable "cidr_block" {
  type = string
  description = "(string, optional) The IPv4 CIDR block for the VPC. CIDR can be explicitly set or it can be derived from IPAM using ipv4_netmask_length"
  default = "10.0.0.0/16"
}

variable "Name" {
  type = string
  description = "(string, required) Name of VPC on AWS"
  default = "Useless Network"
}

variable "availability_zone" {
  type = string
  description = "(string, required) whatever AWS AZ you want to use"
}

variable "subnet_cidr_block" {
  type = string
  description = "(string, optional) The IPv4 CIDR block for the subnet."
  default = "10.0.0.0/24"
}