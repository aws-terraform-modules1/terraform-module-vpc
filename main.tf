resource "aws_vpc" "odai_vpc" {
  cidr_block       = var.cidr_block
  tags = {
    Name = var.Name
  }
}

resource "aws_subnet" "odai_subnet" {
  vpc_id            = aws_vpc.odai_vpc.id
  cidr_block        = var.subnet_cidr_block
  availability_zone = var.availability_zone
}