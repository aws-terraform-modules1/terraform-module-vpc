output "cidr_block" {
  description = "(string, optional) The IPv4 CIDR block for the VPC. CIDR can be explicitly set or it can be derived from IPAM using ipv4_netmask_length"
  value       = aws_vpc.odai_vpc.cidr_block
}

output "subnet_cidr_block" {
  description = "(string, optional) The IPv4 CIDR block for the subnet."
  value       = aws_subnet.odai_subnet.cidr_block
}

output "odai_subnet_id" {
  value = aws_subnet.odai_subnet.id
}

output "VPC_ID" {
  value = aws_vpc.odai_vpc.id
}
